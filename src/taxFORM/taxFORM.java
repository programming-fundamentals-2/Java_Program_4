package taxFORM;
import java.text.DecimalFormat;

import javax.swing.*;

public class taxFORM{
	
	//define members
	//private int zip; 
	private int ssn; 
	private char marital;
	private double income;
	private String name;
	//constructor: required params (ZIP, SSN, MARITAL STATUS, ANNUAL INCOME, NAME)
	
	taxFORM (int z, int s, char m, double i, String n){	
	//zip=z;
	ssn=s;
	marital=m;
	income=i; 
	name= n;
	}
	
	
	//private method. Calculate tax liability
	//Income  Single  Married
	//0 - 20000 15%   15%
	//20001 - 50000 22% 20%
	//50001 - over 30%  28%
	
	private double TaxCalc()
	{	
		switch(Character.toLowerCase(marital)){
		case 's':
			if (income>0 && income<20000){
				return (income*0.15);
			}
			else if (income>20000 && income < 50000){
				return (income*0.22);
			}
			else if (income>50000)
				return (income*0.30);

		case 'm':
			if (income>0 && income<20000){
				return (income*0.15);
			}
			else if (income>20000 && income < 50000){
				return (income*0.20);
			}
			else if (income>50000)
				return (income*0.28);
			
		default: return 0.00;
		}
	}
	
	
	
	
	
	//public method. ReturnTaxForm
		// print formatted tax form, void? infopane?
		// 
		//	NAME           : 
		//	SSN            : 
		//	MARITAL STATUS : 
		//	ANNUAL INCOME  : 
		//	TAX LIABILITY  : 
		//	
	public void ReturnTaxForm(){
		//init decimal format
		DecimalFormat df = new DecimalFormat("#.##");
		
		JOptionPane.showMessageDialog(null,
				"NAME:" +name+ "\n"+
				"SSN: " +ssn+ "\n"+
				"MARITAL STATUS: " +marital+ "\n"+
				"ANNUAL INCOME: " +income+ "\n"+
				"TAX LIABILITY: " +df.format(TaxCalc())+ "\n");
		
	}
	
	
	
	
	
	//public method. ssn verify. (xxx-xx-xxxx)
	public static boolean	ssnVerify (int s){
		if (String.valueOf(s).length() !=9){return false;}
		else return true; //return true if verified
	}
	
	//public method. marital status verify. (m-M, s-S)
	public static boolean	MaritalVerify (char ms){ 
		if (Character.toLowerCase(ms) == 'm'|| Character.toLowerCase(ms) == 's' ) {return true;} //convert to lower case, then check for M, S.
		else return false; //return false if invalid
	} 

	//public method. zip verify.
	public static boolean	zipVerify (int z){
		if (String.valueOf(z).length() !=5){return false;}
		else return true; //return true if verified
	}
		
	
}
