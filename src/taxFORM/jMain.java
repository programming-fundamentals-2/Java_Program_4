package taxFORM;

import java.util.Scanner;

public class jMain {

	public static void main(String[] args) {
		

		//IMPLEMENTATION HERE!
		
		//init vars
		int zip = 0;
		int ssn = 0;
		char marital = ' ';
		//create scanner object
		Scanner input = new Scanner(System.in);

		
		//collect and verify user data
		
				//name <-Validate scanner.NextString
				System.out.print("Please enter your Name: ");
				String name = input.next();
		
				//zip. ZipVerify()<-While Loop until true
				while (taxFORM.zipVerify(zip)!=true){
					System.out.print("Please enter your zip code: ");
					zip = input.nextInt();		
				}
				
				//ssn. ssnVerify()<-While Loop until true
				while (taxFORM.ssnVerify(ssn)!=true){
					System.out.print("Please enter your Social Sec. Number in the format (XXXXXXXXX): ");
					ssn = input.nextInt();		
				}
				
				//marital status. MaritalVerify() <-While Loop until true
				while (taxFORM.MaritalVerify(marital)!=true){
					System.out.print("Please enter your marital status (M-Married, S-Single): ");
					String maritalInput = input.next() ;
					marital = maritalInput.charAt(0);
				}
				
				
				//Annual Income							<-Validate scanner.NextInt
				System.out.print("Please enter your annual income: ");
				int income = input.nextInt();
				
				System.out.println(zip);
				
		//create taxFORM object
		taxFORM t1 = new taxFORM (zip, ssn, marital, income, name);
		
		//display end results
			//taxForm. ReturnTaxForm()
			System.out.print("Form Completed!\n(See Popup Dialog)");
			t1.ReturnTaxForm();
		
		//end
		
	}

}
