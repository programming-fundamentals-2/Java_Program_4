# Java Program #4 Tax Return

**Student:** Liam Hockley

**Submitted Date:** Mar 28, 2012 7:08 pm

**Grade:** 35.0 (max 35.0)

**Instructions**
> Create a TaxReturn class with fields that hold a taxpayer's SSN, last name, first name, > street address, city, state, zip code, annual income, marital status, and tax liability.  > Include a constructor that requires arguments that provide values for all the fields other than the tax liability. The constructor calculates the tax liability based on annual income and the following:
> 
> Income  Single  Married
> 0 - 20000 15%   15%
> 20001 - 50000 22% 20%
> 50001 - over 30%  28%
> 
> In the TaxReturn class, also include a display method that displays all the TaxReturn data.  
> 
> Create an implementation class that prompts the user for the data needed to create a TaxReturn.
> Continue to prompt the user for data as long as any of the following are true:
> 
> - SSN is not the correct format - digits and dashes 999-99-9999
> -Zip code not five digits
> -Marital status does not begin with one of the following "S", "s", "M", "m".
> - Annual income is negative.
> 
> After all the input is correct - display the TaxReturns objects values.